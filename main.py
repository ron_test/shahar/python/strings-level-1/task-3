def count_repeated_characters(input_string):
    repeated_chars = set()
    unique_chars = set()

    for char in input_string:
        if char in unique_chars:
            repeated_chars.add(char)
        else:
            unique_chars.add(char)

    for char in repeated_chars:
        count = input_string.count(char)
        print(f"{char} {count}")


input_string = input("Enter a string: ")
count_repeated_characters(input_string)
